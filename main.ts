import { DAppClient, NetworkType } from "@airgap/beacon-sdk";

async function main() {

    console.log("hello");
    
    const client = new DAppClient({ name: "just testing"});

    console.log("client", client);

    const permissions = await client.requestPermissions({
	network: {
	    type: NetworkType.DELPHINET,
	}
    });

    console.log("permissions", permissions);
}

main()
