import commonjs from '@rollup/plugin-commonjs';
import nodeResolve from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';
import json from '@rollup/plugin-json';
import nodePolyfills from 'rollup-plugin-node-polyfills';
import replace from '@rollup/plugin-replace';


export default {
    input: "main.ts",

    output: {
	file: "bundle.js",
	format: "iife",
    },

    plugins: [
	/* Enable this code to patch the code to work.
	replace({
	    "qr = qrcode(": "qr = qrcode.default(",
	    delimiters: ["", ""],
	    include: "node_modules/@airgap/beacon-sdk/**",
	}),
	*/

	commonjs(),
	nodePolyfills(),
	nodeResolve({
	    browser: true,
	}),
	json(),
	typescript(),
    ],

    context: "window",
}
